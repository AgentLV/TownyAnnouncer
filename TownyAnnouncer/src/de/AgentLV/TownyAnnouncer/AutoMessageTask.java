package de.AgentLV.TownyAnnouncer;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import de.AgentLV.TownyAnnouncer.Files.ConfigAccessor;
import de.AgentLV.TownyAnnouncer.Files.Messages;

public class AutoMessageTask {
	
	private static TownyAnnouncer plugin;
	private static ConfigAccessor cConfig;
	private static ConfigAccessor cMessage;
	private static Messages msg;
	
	public AutoMessageTask(TownyAnnouncer plugin) {
		AutoMessageTask.plugin = plugin;
		cConfig = new ConfigAccessor(plugin, "config.yml");
		cMessage = new ConfigAccessor(plugin, "Messages.yml");
		msg = new Messages(plugin);
	}
	
	public static void stopScheduleTasks() {
		plugin.getServer().getScheduler().cancelTasks(plugin);
	}
	
	public static void startScheduleTasks() {
		
		cMessage.reloadConfig();
		cConfig.reloadConfig();
		
		
		final ArrayList<String> keys = new ArrayList<String>();
		
		for (String key : cMessage.getConfig().getKeys(false)) {
			
			if (cMessage.getConfig().getConfigurationSection(key).getBoolean("enabled"))
				keys.add(key);
		}
		
		final int time = cConfig.getConfig().getInt("interval");
		final String prefix = ChatColor.translateAlternateColorCodes('&', msg.getString("prefix")) + " ";
		
		//AutoMessage Task
		plugin.getServer().getScheduler().runTaskTimerAsynchronously(plugin, new Runnable() {
			int i = 0;
			
			@Override
			public void run() {
				
				if (i >= keys.size()) {
					
					keys.clear();
					cMessage.reloadConfig();
					
					for (String key : cMessage.getConfig().getKeys(false) ) {
						
						if (cMessage.getConfig().getConfigurationSection(key).getBoolean("enabled"))
							keys.add(key);
						
					}
					
					i = 0;
				}
				
				if (keys.size() == 0)
					return;
				
				String broadcast = prefix + cMessage.getConfig().getConfigurationSection(keys.get(i)).getString("message");
			
				if (cConfig.getConfig().getBoolean("playerUseColorCodes"))
					broadcast = ChatColor.translateAlternateColorCodes('&', broadcast);
				
				if (!cConfig.getConfig().getBoolean("bungee")) {
					plugin.getServer().broadcastMessage(broadcast);
				} else if (Bukkit.getOnlinePlayers().size() != 0){
					BungeecordHandler.getBungeePlayerList(broadcast);
				}
				
				if (cConfig.getConfig().getBoolean("log-to-console"))
					plugin.getLogger().info(broadcast);
					
				++i;
			}
			//Seconds*Ticks
		}, time*20, time*20);
	}
	

}
