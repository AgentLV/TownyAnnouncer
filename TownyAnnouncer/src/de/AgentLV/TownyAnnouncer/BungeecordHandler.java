package de.AgentLV.TownyAnnouncer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class BungeecordHandler {

	private static TownyAnnouncer plugin;
	private static String broadcast;
	
	public BungeecordHandler(TownyAnnouncer plugin) {
		BungeecordHandler.plugin = plugin;
	}
	
	public static void getBungeePlayerList(String broadcast) {
		
		BungeecordHandler.broadcast = broadcast;
		
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("PlayerList");
		out.writeUTF("ALL");

		Player player = Iterables.getFirst(Bukkit.getOnlinePlayers(), null);
		player.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
	}
	
	public static void broadcastAutoMessage(String server, String[] playerList) {
		
		for (String playerName : playerList) {
			
			ByteArrayDataOutput out = ByteStreams.newDataOutput();
			
			out.writeUTF("Message");
			out.writeUTF(playerName);
			out.writeUTF(broadcast);
			
			Player player = Iterables.getFirst(Bukkit.getOnlinePlayers(), null);
			player.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
		}
	}
	
}
