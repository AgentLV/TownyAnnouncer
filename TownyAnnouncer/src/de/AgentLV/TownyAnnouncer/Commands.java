package de.AgentLV.TownyAnnouncer;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.TownyUniverse;

import de.AgentLV.TownyAnnouncer.Files.ConfigAccessor;
import de.AgentLV.TownyAnnouncer.Files.Messages;

public class Commands implements CommandExecutor {
	
	private TownyAnnouncer plugin;
	private ConfigAccessor cMessage;
	private Messages msg;
	private String myFalse;
	
	public Commands(TownyAnnouncer plugin) {
		this.plugin = plugin;
		cMessage = new ConfigAccessor(this.plugin, "Messages.yml");
		msg = new Messages(plugin);
	}
	
	private void pluginDescription(CommandSender sender) {
		String mainAuthor = plugin.getDescription().getAuthors().get(0);
		String pluginName = plugin.getDescription().getName();
		String pluginVersion = plugin.getDescription().getVersion();
		
		sender.sendMessage("§c" + pluginName + "§7 v" + pluginVersion + " by §c" + mainAuthor);
		sender.sendMessage("§7Commands: /tan help");
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		myFalse = msg.getString("mayorOnly");
		Resident r = null;
		String townName = null;
		
		if (sender instanceof Player) {
			
			try {
				r = TownyUniverse.getDataSource().getResident(sender.getName());
			} catch (NotRegisteredException e) { }
			
			try {
				townName = r.getTown().getName();
			} catch (NotRegisteredException e) { }
		}
		
		if (cmd.getName().equalsIgnoreCase("TownyAnnouncer")) {
			
			if ( args.length == 0 ) {
				
				pluginDescription(sender);
				
			} else if (args[0].equalsIgnoreCase("help")) {
				
				sender.sendMessage("");
				sender.sendMessage("§8---- §c§lTownyMessage " + msg.getString("help.Commands") + " §r§8----" );
				sender.sendMessage("");
				sender.sendMessage("§a/tan create <" + msg.getString("help.create_parameter") + "> §7§ " + msg.getString("help.create"));
				sender.sendMessage("§a/tan enable §7§ " + msg.getString("help.enable"));
				sender.sendMessage("§a/tan disable §7§ " + msg.getString("help.disable"));
				sender.sendMessage("§a/tan view [" + msg.getString("help.view_parameter") + "] §7§ " + msg.getString("help.view"));
				sender.sendMessage("§a/tan remove §7§ " + msg.getString("help.remove"));
				
				if (sender.hasPermission("townyannouncer.admin"))
					sender.sendMessage("§a/tan reload §7§ " + msg.getString("help.reload"));
				
				sender.sendMessage("");
				
			} else if (args[0].equalsIgnoreCase("create")) {
				
				if (sender instanceof Player) {
					
					if (r != null && r.isMayor()) {
					
						if (args.length > 1) {
							
							String Message = args[1];
							for (int i = 2; i < args.length; ++i) {
								Message += " " + args[i];
							}
							
							if (cMessage.getConfig().getConfigurationSection(townName) == null) {
							
								cMessage.getConfig().createSection(townName);
								cMessage.getConfig().getConfigurationSection(townName).set("message", Message);
								cMessage.getConfig().getConfigurationSection(townName).set("enabled", true);
								
							} else {
								cMessage.getConfig().getConfigurationSection(townName).set("message", Message);
							}
							
							cMessage.saveConfig();
							sender.sendMessage(msg.getString("create.AutoMessageAdded"));
							
						} else {
							sender.sendMessage(msg.getString("create.provideMessage"));
						}
						
					} else {
						sender.sendMessage(myFalse);
					}
					
				} else {
					sender.sendMessage(msg.getString("create.playerOnly"));
				}
				
			} else if (args[0].equalsIgnoreCase("enable")) {
				
				if (!sender.hasPermission("townyannouncer.admin")) {
					
					if (r != null && r.isMayor()) {
					
						if (cMessage.getConfig().getConfigurationSection(townName) != null) {
							
							cMessage.getConfig().getConfigurationSection(townName).set("enabled", true);
							cMessage.saveConfig();
							sender.sendMessage(msg.getString("enable.activated"));
							
						} else {
							sender.sendMessage(msg.getString("yourCityNoMSG"));
						}
						
					} else {
						sender.sendMessage(myFalse);
					}
					
				} else {
					
					if (args.length == 1) {
						
						if (r != null && r.isMayor()) {
							
							if (cMessage.getConfig().getConfigurationSection(townName) != null) {
								
								cMessage.getConfig().getConfigurationSection(townName).set("enabled", true);
								cMessage.saveConfig();
								sender.sendMessage(msg.getString("enable.activated"));
								
							} else {
								sender.sendMessage(msg.getString("yourCityNoMSG"));
							}
							
						} else {
							sender.sendMessage(msg.getString("provideCity"));
						}
						
					} else {
						
						if (cMessage.getConfig().getConfigurationSection(args[1]) != null) {
							
							cMessage.getConfig().getConfigurationSection(args[1]).set("enabled", true);
							cMessage.saveConfig();
							sender.sendMessage(msg.getString("enable.activated_city").replaceAll("%city%", args[1]));
							
						} else {
							sender.sendMessage(msg.getString("cityNoMSG").replaceAll("%city%", args[1]));
						}	
					}
				}
				
			} else if (args[0].equalsIgnoreCase("disable")) {
				
				if (!sender.hasPermission("townyannouncer.admin")) {
					
					if (r != null && r.isMayor()) {
					
						if (cMessage.getConfig().getConfigurationSection(townName) != null) {
							
							cMessage.getConfig().getConfigurationSection(townName).set("enabled", false);
							cMessage.saveConfig();
							sender.sendMessage(msg.getString("disable.deactivated"));
							
						} else {
							sender.sendMessage(msg.getString("yourCityNoMSG"));
						}
						
					} else {
						sender.sendMessage(myFalse);
					}
					
				} else {
					
					if (args.length == 1) {

						if (r != null && r.isMayor()) {
					
							if (cMessage.getConfig().getConfigurationSection(townName) != null) {
								
								cMessage.getConfig().getConfigurationSection(townName).set("enabled", false);
								cMessage.saveConfig();
								sender.sendMessage(msg.getString("disable.deactivated"));
								
							} else {
								sender.sendMessage(msg.getString("yourCityNoMSG"));
							}
						} else {
							sender.sendMessage(msg.getString("provideCity"));
						}

					} else {
						
						if (cMessage.getConfig().getConfigurationSection(args[1]) != null) {
							
							cMessage.getConfig().getConfigurationSection(args[1]).set("enabled", false);
							cMessage.saveConfig();
							sender.sendMessage(msg.getString("disable.deactivated_city").replaceAll("%city%", args[1]));
							
						} else {
							sender.sendMessage(msg.getString("cityNoMSG").replaceAll("%city%", args[1]));
						}
					}	
				}
				
			} else if (args[0].equalsIgnoreCase("view")) {
				
				if (args.length == 1) {
					
					if (sender instanceof Player) {
						
						if (townName != null) {
						
							if (cMessage.getConfig().getConfigurationSection(townName) != null) {
								
								if (cMessage.getConfig().getConfigurationSection(townName).getBoolean("enabled")) {
									sender.sendMessage(msg.getString("enable.activated"));
								} else {
									sender.sendMessage(msg.getString("disable.deactivated"));
								}
								
								sender.sendMessage(msg.getString("view.yourCity").replaceAll("%msg%", cMessage.getConfig().getConfigurationSection(townName).getString("message")));
								
							} else {
								sender.sendMessage(msg.getString("yourCityNoMSG"));
							}
							
						} else {
							sender.sendMessage(msg.getString("provideCity"));
						}
						
					} else {
						sender.sendMessage(msg.getString("provideCity"));
					}
					
				} else {
					
					if (cMessage.getConfig().getConfigurationSection(args[1]) != null) {
						
						if (cMessage.getConfig().getConfigurationSection(args[1]).getBoolean("enabled")) {
							sender.sendMessage(msg.getString("enable.activated_city").replaceAll("%city%", args[1]));
						} else {
							sender.sendMessage(msg.getString("disable.deactivated_city").replaceAll("%city%", args[1]));
						}
						sender.sendMessage(msg.getString("view.otherCity").replaceAll("%msg%", cMessage.getConfig().getConfigurationSection(args[1]).getString("message"))
								.replaceAll("%city%", args[1]));
						
					} else {
						sender.sendMessage((msg.getString("cityNoMSG").replaceAll("%city%", args[1])));
					}
				}
				
			} else if (args[0].equalsIgnoreCase("remove")) {
				
				if (!sender.hasPermission("townyannouncer.admin")) {
							
					if (r != null && r.isMayor()) {
					
						if (cMessage.getConfig().getConfigurationSection(townName) != null) {
							
							cMessage.getConfig().set(townName, null);
							cMessage.saveConfig();
							sender.sendMessage(msg.getString("remove.removed"));
							
						} else {
							sender.sendMessage(msg.getString("yourCityNoMSG"));
						}
					
					} else {
						sender.sendMessage(myFalse);
					}
						
				} else {
					
					if (args.length == 1) {
						
						if (r != null && r.isMayor()) {
							
							if (cMessage.getConfig().getConfigurationSection(townName) != null) {
								
								cMessage.getConfig().set(townName, null);
								cMessage.saveConfig();
								sender.sendMessage(msg.getString("remove.removed"));
								
							} else {
								sender.sendMessage(msg.getString("yourCityNoMSG"));
							}
						
						} else {
							sender.sendMessage(msg.getString("provideCity"));
						}
					
					} else {
						
						if (cMessage.getConfig().getConfigurationSection(args[1]) != null) {
							
							cMessage.getConfig().set(args[1], null);
							cMessage.saveConfig();
							sender.sendMessage(msg.getString("remove.removed_city").replaceAll("%city%", args[1]));
							
						} else {
							sender.sendMessage(msg.getString("cityNoMSG").replaceAll("%city%", args[1]));
						}
					}
					
				}
				
			} else if (args[0].equalsIgnoreCase("reload")) {
				
				if (sender.hasPermission("townyannouncer.admin")) {
				
					AutoMessageTask.stopScheduleTasks();
					cMessage.reloadConfig();
					msg.reload();
					AutoMessageTask.startScheduleTasks();
					sender.sendMessage(msg.getString("reload.reload"));
					
				} else {
					sender.sendMessage(msg.getString("reload.noPerms"));
				}
				
			} else {
				pluginDescription(sender);
			}
		}
		
		return true;
	}
}
