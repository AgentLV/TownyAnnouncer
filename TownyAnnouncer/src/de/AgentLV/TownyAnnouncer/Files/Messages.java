package de.AgentLV.TownyAnnouncer.Files;

import org.bukkit.ChatColor;

import de.AgentLV.TownyAnnouncer.TownyAnnouncer;

public class Messages {

	private static TownyAnnouncer plugin;
	private static ConfigAccessor cLocale;
	
	public Messages(TownyAnnouncer plugin) {
		Messages.plugin = plugin;
		cLocale = new ConfigAccessor(plugin, "lang.yml");
	}
	
	public String getString(String s) {
		try {
			return ChatColor.translateAlternateColorCodes('&', cLocale.getConfig().getString(s));
		} catch(NullPointerException e) {
			plugin.getLogger().warning("�4Could not get String in locale.yml �c" + s);
			return "";
		}
	}
	
	public void reload() {
		cLocale.reloadConfig();
	}
	
}
