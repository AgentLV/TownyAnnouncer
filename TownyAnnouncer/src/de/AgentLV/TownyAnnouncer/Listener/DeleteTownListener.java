package de.AgentLV.TownyAnnouncer.Listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.palmergames.bukkit.towny.event.DeleteTownEvent;

import de.AgentLV.TownyAnnouncer.TownyAnnouncer;
import de.AgentLV.TownyAnnouncer.Files.ConfigAccessor;

public class DeleteTownListener implements Listener {

	TownyAnnouncer plugin;
	ConfigAccessor c;
	
	public DeleteTownListener(TownyAnnouncer plugin) {
		this.plugin = plugin;
		c = new ConfigAccessor(this.plugin, "Messages.yml");
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onNationRemove(DeleteTownEvent e) {
		
		String townName = e.getTownName();
				
		if (c.getConfig().getConfigurationSection(townName) != null) {
			
			c.getConfig().set(townName, null);
			c.saveConfig();
			
		}
	}
}
