package de.AgentLV.TownyAnnouncer.Listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.palmergames.bukkit.towny.event.NationRemoveTownEvent;

import de.AgentLV.TownyAnnouncer.TownyAnnouncer;
import de.AgentLV.TownyAnnouncer.Files.ConfigAccessor;

public class NationRemoveTownListener implements Listener {

	TownyAnnouncer plugin;
	ConfigAccessor c;
	
	public NationRemoveTownListener(TownyAnnouncer plugin) {
		this.plugin = plugin;
		c = new ConfigAccessor(this.plugin, "Messages.yml");
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onNationRemove(NationRemoveTownEvent e) {
		
		String townName = e.getTown().getName();
				
		if (c.getConfig().getConfigurationSection(townName) != null) {
			
			c.getConfig().set(townName, null);
			c.saveConfig();
			
		}
	}
}
