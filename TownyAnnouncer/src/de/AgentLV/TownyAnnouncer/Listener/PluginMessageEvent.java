package de.AgentLV.TownyAnnouncer.Listener;

import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import de.AgentLV.TownyAnnouncer.BungeecordHandler;
import de.AgentLV.TownyAnnouncer.TownyAnnouncer;

public class PluginMessageEvent implements PluginMessageListener {
	
	public PluginMessageEvent(TownyAnnouncer plugin) {
		plugin.getServer().getMessenger().registerIncomingPluginChannel(plugin, "BungeeCord", this);
	}

	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		
	    if (!channel.equals("BungeeCord"))
	      return;
	    
	    ByteArrayDataInput in = ByteStreams.newDataInput(message);
	    String subchannel = in.readUTF();
	    if (subchannel.equals("PlayerList")) {
	    	String server = in.readUTF();
	    	String[] playerList = in.readUTF().split(", ");
	    	
	    	BungeecordHandler.broadcastAutoMessage(server, playerList);
	    }
  }
	
}
