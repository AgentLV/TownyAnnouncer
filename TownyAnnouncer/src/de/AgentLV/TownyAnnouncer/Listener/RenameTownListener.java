package de.AgentLV.TownyAnnouncer.Listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.palmergames.bukkit.towny.event.RenameTownEvent;

import de.AgentLV.TownyAnnouncer.TownyAnnouncer;
import de.AgentLV.TownyAnnouncer.Files.ConfigAccessor;

public class RenameTownListener implements Listener {
	
	TownyAnnouncer plugin;
	ConfigAccessor c;
	
	public RenameTownListener(TownyAnnouncer plugin) {
		this.plugin = plugin;
		c = new ConfigAccessor(this.plugin, "Messages.yml");
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onRenameTown(RenameTownEvent e) {
		
		if (c.getConfig().getConfigurationSection(e.getOldName()) != null) {
			
			String townName = e.getTown().getName();
			String message = c.getConfig().getConfigurationSection(e.getOldName()).getString("message");
			boolean enabled = c.getConfig().getConfigurationSection(e.getOldName()).getBoolean("enabled");
			
			c.getConfig().createSection(townName);
			
			if (message != null) {
				c.getConfig().getConfigurationSection(townName).set("message", message);
			} else {
				c.getConfig().getConfigurationSection(townName).set("message", "");
			}
			
			if (enabled == true) {
				c.getConfig().getConfigurationSection(townName).set("enabled", true);
			} else {
				c.getConfig().getConfigurationSection(townName).set("enabled", false);
			}
			
			c.getConfig().set(e.getOldName(), null);
			c.saveConfig();
		}
	}
}
