package de.AgentLV.TownyAnnouncer;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.palmergames.bukkit.towny.Towny;
import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownyUniverse;

import de.AgentLV.TownyAnnouncer.Files.ConfigAccessor;
import de.AgentLV.TownyAnnouncer.Listener.DeleteTownListener;
import de.AgentLV.TownyAnnouncer.Listener.NationRemoveTownListener;
import de.AgentLV.TownyAnnouncer.Listener.PluginMessageEvent;
import de.AgentLV.TownyAnnouncer.Listener.RenameTownListener;

public class TownyAnnouncer extends JavaPlugin {
	
	private ConfigAccessor cConfig;
	private ConfigAccessor cMessage;
	private ConfigAccessor cLang;
	
	@Override
	public void onEnable() {
		Towny towny = (Towny) getServer().getPluginManager().getPlugin("Towny");
		
		if (towny == null) {
			getLogger().severe("Towny not found! Disabling TownyAnnouncer...");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		
		new RenameTownListener(this);
		new NationRemoveTownListener(this);
		new DeleteTownListener(this);
		new AutoMessageTask(this);
		new BungeecordHandler(this);
		
		initFiles();
		getCommand("TownyAnnouncer").setExecutor(new Commands(this));
		AutoMessageTask.startScheduleTasks();
		
		if (cConfig.getConfig().getBoolean("bungee")) {
			this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
			new BungeecordHandler(this);
			new PluginMessageEvent(this);
		}
		
		checkTowns();
		
	}
	
	private void initFiles() {
		cMessage = new ConfigAccessor(this, "Messages.yml");
		cMessage.reloadConfig();
		cMessage.saveConfig();
		
		cConfig = new ConfigAccessor(this, "config.yml");
		cConfig.reloadConfig();
		cConfig.saveConfig();
		
		cLang = new ConfigAccessor(this, "lang.yml");
		cLang.reloadConfig();
		cLang.saveConfig();
	}
	
	private void checkTowns() {
		
		for (String key : cMessage.getConfig().getKeys(false)) {
			
			try {
			
				Town town = TownyUniverse.getDataSource().getTown(key);
				
				if (town == null)
					cMessage.getConfig().set(key, null);
				
			} catch (NotRegisteredException e) {
				cMessage.getConfig().set(key, null);
			}
			
		}
	}
	
}